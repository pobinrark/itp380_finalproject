// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "MyProject3.h"
#include "MyProject3GameMode.h"
#include "MyProject3Ball.h"

AMyProject3GameMode::AMyProject3GameMode()
{
	// set default pawn class to our ball
	DefaultPawnClass = AMyProject3Ball::StaticClass();
}
